import CoreData
import UIKit

class ListViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate {
  @IBOutlet weak var tableView: UITableView!
  
  private var fetchedResultsController: NSFetchedResultsController<Task>?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.title = NSLocalizedString("ListViewController.NavigationItem.Title", comment: "ListViewController.NavigationItem.Title")
    navigationController?.navigationBar.prefersLargeTitles = true
    
    Database.shared.performForegroundTask { [weak self] output in
      do {
        let session = try output.value()
        let fetchRequest: NSFetchRequest<Task> = Task.fetchRequest()
        
        fetchRequest.sortDescriptors = [
          NSSortDescriptor(
            key: "name",
            ascending: true
          )
        ]
        
        self?.fetchedResultsController = NSFetchedResultsController(
          fetchRequest: fetchRequest,
          managedObjectContext: session.foregroundContext,
          sectionNameKeyPath: nil,
          cacheName: nil
        )
        
        self?.tableView.dataSource = self
        self?.tableView.delegate = self
        self?.fetchedResultsController?.delegate = self
        try self?.fetchedResultsController?.performFetch()
        self?.tableView.reloadData()
      }
      catch {
        print(error)
      }
    }
  }
  
  func configure(_ cell: UITableViewCell, at indexPath: IndexPath) {
    cell.textLabel?.text = fetchedResultsController?.object(at: indexPath).name
  }
  
  func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    tableView.endUpdates()
  }
  
  func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    tableView.beginUpdates()
  }
  
  func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
    switch type {
    case .delete:
      guard let indexPath = indexPath else {
        return
      }
      
      tableView.deleteRows(at: [indexPath], with: .automatic)
    case .insert:
      guard let newIndexPath = newIndexPath else {
        return
      }
      
      tableView.insertRows(at: [newIndexPath], with: .automatic)
    case .move:
      guard let indexPath = indexPath else {
        return
      }
      
      guard let newIndexPath = newIndexPath else {
        return
      }
      
      tableView.deleteRows(at: [indexPath], with: .automatic)
      tableView.insertRows(at: [newIndexPath], with: .automatic)
    case .update:
      guard let indexPath = indexPath else {
        return
      }
      
      guard let cell = tableView.cellForRow(at: indexPath) else {
        return
      }
      
      configure(cell, at: indexPath)
    }
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return fetchedResultsController?.sections?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return fetchedResultsController?.sections?[section].numberOfObjects ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ListTaskCell", for: indexPath)
    configure(cell, at: indexPath)
    return cell
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    switch editingStyle {
    case .delete:
      guard let id = fetchedResultsController?.object(at: indexPath).objectID else {
        return
      }
      
      ListManager.shared.deleteTask(havingID: id)
    default:
      break
    }
  }
  
  @IBAction func addTask(_ sender: UIBarButtonItem) {
    ListManager.shared.insertTask()
  }
}
