import CoreData

class ListManager {
  private init() {
    
  }
  
  static var shared: ListManager = {
    return ListManager()
  }()
  
  func deleteTask(havingID id: NSManagedObjectID) {
    Database.shared.performBackgroundTask { output in
      do {
        let session = try output.value()
        let object = session.backgroundContext.object(with: id)
        session.backgroundContext.delete(object)
        
        guard session.backgroundContext.hasChanges else {
          return
        }
        
        try session.backgroundContext.save()
      }
      catch {
        print(error)
      }
    }
  }
  
  func insertTask() {
    Database.shared.performBackgroundTask { output in
      do {
        let session = try output.value()
        let task = Task(context: session.backgroundContext)
        task.name = UUID().uuidString
        
        guard session.backgroundContext.hasChanges else {
          return
        }
        
        try session.backgroundContext.save()
      }
      catch {
        print(error)
      }
    }
  }
}
