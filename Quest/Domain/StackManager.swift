import CoreData

class StackManager {
  private init() {
    
  }
  
  static var shared: StackManager = {
    return StackManager()
  }()
  
  func deleteList(havingID id: NSManagedObjectID) {
    Database.shared.performBackgroundTask { output in
      do {
        let session = try output.value()
        let object = session.backgroundContext.object(with: id)
        session.backgroundContext.delete(object)
        
        guard session.backgroundContext.hasChanges else {
          return
        }
        
        try session.backgroundContext.save()
      }
      catch {
        print(error)
      }
    }
  }
  
  func insertList() {
    Database.shared.performBackgroundTask { output in
      do {
        let session = try output.value()
        let list = List(context: session.backgroundContext)
        list.name = UUID().uuidString
        
        guard session.backgroundContext.hasChanges else {
          return
        }
        
        try session.backgroundContext.save()
      }
      catch {
        print(error)
      }
    }
  }
}
