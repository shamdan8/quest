import CoreData

class DatabaseBackgroundSession {
  init(backgroundContext: NSManagedObjectContext, foregroundContext: NSManagedObjectContext) {
    self.backgroundContext = backgroundContext
    self.foregroundContext = foregroundContext
  }
  
  let backgroundContext: NSManagedObjectContext
  let foregroundContext: NSManagedObjectContext
}
