//import CoreData
//
//class DatabaseTester {
//  private init() {
//  
//  }
//  
//  static var shared: DatabaseTester = {
//    return DatabaseTester()
//  }()
//  
//  func deleteObject(havingID id: NSManagedObjectID) {
//    Database.shared.performBackgroundTask { output in
//      do {
//        let session = try output.value()
//        let object = session.backgroundContext.object(with: id)
//        session.backgroundContext.delete(object)
//        
//        guard session.backgroundContext.hasChanges else {
//          return
//        }
//        
//        try session.backgroundContext.save()
//      }
//      catch {
//        print(error)
//      }
//    }
//  }
//  
//  func deleteAllObjects() {
//    Database.shared.performBackgroundTask { output in
//      do {
//        let session = try output.value()
//        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = Item.fetchRequest()
//        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//        batchDeleteRequest.resultType = .resultTypeObjectIDs
//        let result = try session.backgroundContext.execute(batchDeleteRequest) as? NSBatchDeleteResult
//        
//        guard let objectIDs = result?.result as? [NSManagedObjectID] else {
//          return
//        }
//        
//        let changes = [NSDeletedObjectsKey: objectIDs]
//        NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [session.foregroundContext])
//      }
//      catch {
//        print(error)
//      }
//    }
//  }
//  
//  func getAllObjects() {
//    Database.shared.performBackgroundTask { output in
//      do {
//        let session = try output.value()
//        let fetchRequest: NSFetchRequest<Item> = Item.fetchRequest()
//        
//        let asynchronousFetchRequest = NSAsynchronousFetchRequest(fetchRequest: fetchRequest) { result in
//          guard let items = result.finalResult else {
//            return
//          }
//          
//          for item in items {
//            print(item)
//          }
//        }
//        
//        try session.backgroundContext.execute(asynchronousFetchRequest)
//      }
//      catch {
//        
//      }
//    }
//  }
//  
//  func insertObject() {
//    Database.shared.performBackgroundTask { output in
//      do {
//        let session = try output.value()
//        let item = Item(context: session.backgroundContext)
//        item.name = UUID().uuidString
//        
//        guard session.backgroundContext.hasChanges else {
//          return
//        }
//        
//        try session.backgroundContext.save()
//      }
//      catch {
//        print(error)
//      }
//    }
//  }
//  
//  func updateObject(havingID id: NSManagedObjectID) {
//    Database.shared.performBackgroundTask { output in
//      do {
//        let session = try output.value()
//        
//        guard let object = session.backgroundContext.object(with: id) as? Item else {
//          return
//        }
//        
//        object.name = UUID().uuidString
//        
//        guard session.backgroundContext.hasChanges else {
//          return
//        }
//        
//        try session.backgroundContext.save()
//      }
//      catch {
//        print(error)
//      }
//    }
//  }
//}
