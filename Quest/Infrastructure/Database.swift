import CoreData

class Database {
  private init() {
    queue = DispatchQueue(label: "com.azardarr.database.queue")
  }
  
  static var shared: Database = {
    return Database()
  }()
  
  private var cachedPersistentContainer: CustomPersistentContainer?
  private let queue: DispatchQueue
  
  func performBackgroundTask(_ outputHandler: @escaping (Output<DatabaseBackgroundSession>) -> Void) {
    resolvedPersistentContainer { output in
      do {
        let persistentContainer = try output.value()
        
        persistentContainer.performBackgroundTask { backgroundContext in
          let session = DatabaseBackgroundSession(
            backgroundContext: backgroundContext,
            foregroundContext: persistentContainer.viewContext
          )
          
          Output(session).handle(with: outputHandler)
        }
      }
      catch {
        Output(error).handle(with: outputHandler)
      }
    }
  }
  
  func performForegroundTask(_ outputHandler: @escaping (Output<DatabaseForegroundSession>) -> Void) {
    resolvedPersistentContainer { output in
      do {
        let persistentContainer = try output.value()
        
        let session = DatabaseForegroundSession(
          viewContext: persistentContainer.viewContext
        )
        
        DispatchQueue.main.async {
          Output(session).handle(with: outputHandler)
        }
      }
      catch {
        Output(error).handle(with: outputHandler)
      }
    }
  }
  
  private func resolvedPersistentContainer(_ outputHandler: @escaping OutputHandler<CustomPersistentContainer>) {
    queue.async { [weak self] in
      if let persistentContainer = self?.cachedPersistentContainer {
        DispatchQueue.global().async {
          Output(persistentContainer).handle(with: outputHandler)
        }
      }
      else {
        self?.newPersistentContainer { [weak self] output in
          do {
            let persistentContainer = try output.value()
            self?.cachedPersistentContainer = persistentContainer
            
            DispatchQueue.global().async {
              Output(persistentContainer).handle(with: outputHandler)
            }
          }
          catch {
            Output(error).handle(with: outputHandler)
          }
        }
      }
    }
  }
  
  private func newPersistentContainer(_ outputHandler: @escaping OutputHandler<CustomPersistentContainer>) {
    let dispatchGroup = DispatchGroup()
    let persistentContainer = CustomPersistentContainer(name: "Quest")
    
    for persistentStoreDescription in persistentContainer.persistentStoreDescriptions {
      dispatchGroup.enter()
      persistentStoreDescription.shouldAddStoreAsynchronously = true
    }
    
    var error: Error?
    
    persistentContainer.loadPersistentStores { (storeDescription, storeError) in
      if let storeError = storeError {
        #warning("todo: create a composite error")
        error = storeError
      }
      
      dispatchGroup.leave()
    }
    
    dispatchGroup.wait()
    
    if let error = error {
      Output(error).handle(with: outputHandler)
    }
    else {
      persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
      Output(persistentContainer).handle(with: outputHandler)
    }
  }
}

private class CustomPersistentContainer: NSPersistentContainer {
  
}
