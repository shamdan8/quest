import CoreData

class DatabaseForegroundSession {
  init(viewContext: NSManagedObjectContext) {
    self.foregroundContext = viewContext
  }
  
  let foregroundContext: NSManagedObjectContext
}
